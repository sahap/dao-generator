package com.psaha;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;

public class AbatorGenerator {

	public static void main(String[] args) {
		try{
			List<String> warnings = new ArrayList<String>();
			boolean overwrite = true;
			File configFile = new File(".//resource//generator.xml");
			ConfigurationParser cp = new ConfigurationParser(warnings);
			Configuration config = cp.parseConfiguration(configFile);
			DefaultShellCallback callback = new DefaultShellCallback(overwrite);
			MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
			myBatisGenerator.generate(null);
		}
		catch(IOException io){
			System.err.println(io);
		}
		catch(XMLParserException xp){
			System.err.println(xp);
		}
		catch(InvalidConfigurationException ic){
			System.err.println(ic);
		}
		catch(InterruptedException ie){
			System.err.println(ie);
		}
		catch(SQLException se){
			System.err.println(se);
		}
	}
}
